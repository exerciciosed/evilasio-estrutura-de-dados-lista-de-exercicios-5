

object Main {
	def main(args : Array[String]){

		
		
		//1 - Criando a estrutura de dados
	  val numberOfElements = 10
		var myHeap = new Heap(numberOfElements)
	  println("Heap de 10 posi��es criado")
	  println()
		
		//2 - Inserindo elementos
		println("Inserindo 10 valores")
	  for(i<-1 to numberOfElements) 
		  myHeap.insert(i)
		myHeap.show()
		println()

		//3 - Buscando elemento
		println("Buscando valor 8")
		var result = myHeap.search(8)
		result.==(-1) match{ 
		  case true => println("Valor n�o encontrado!")  
		  case false => println("Valor encontrado na posi��o: "+result)
		}
		println()
		
		
		//4 - Removendo elemento
		println("Remo��o")
		myHeap.remove()
		myHeap.show()
		println()
		
		//5 - Alterar valor de determinado elemento
		println("Trocando elemento de valor 3 por 99")
		myHeap.changeValue(3, 99)
		myHeap.show()
		println()
		
		//6 - Liberando a estrutura de dados
		myHeap.free()
		println("Heap liberado")
	}
}