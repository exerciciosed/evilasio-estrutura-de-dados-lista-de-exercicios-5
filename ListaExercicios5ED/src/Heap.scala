class Heap(var max : Int, var pos:Int, var priority : Array[Int]) {
  
  def this(lenght : Int){
		this(lenght, 0, new Array[Int](lenght))
	}

	def insert(priority : Int){
		  pos.<=(max-1) match {
		  case true  => this.priority(pos) = priority
					          reviseAbove(pos)
					          pos += 1
		  case false => printf("Heap Cheio!")
		}
	}

	def reviseAbove(pos : Int){
		var position = pos
		while(position > 0){
			var father = (position -1)/2
					priority(father).<(this.priority(position)) match{
			      case true =>	swap(position, father, this.priority)
			      case false => return
			      }
			position = father
		}
	}

	def swap(a : Int, b : Int, v : Array[Int]){
		var f  = v(a)
		v(a) = v(b)
		v(b) = f
	}

	def remove() : Int = {
			this.pos.>(0) match {
			  case true =>	var top = this.priority(0)
						          this.priority(0) = this.priority(this.pos-1)
						          this.pos -= 1
						          reviseBelow(0)
						          return top
			  case false =>	printf("Heap vazio!")
				              return -1
			}
	}

	def reviseBelow(position:Int){
		var father = position
		while((2*father+1) < this.pos){
			var leftSon = 2 * father + 1
			var rightSon = 2 * father + 2
			var son = 0

			if(rightSon >= this.pos) rightSon = leftSon

			this.priority(leftSon).>(this.priority(rightSon)) match {
			  case true => son = leftSon
			  case false =>	son = rightSon
			}

			this.priority(father).<(this.priority(son)) match {
			  case true => swap(father, son, this.priority)
			  case false =>	return
			}
			
			father = son
		}

	}

	def free(){
		this.priority = new Array[Int](max)
	}

	def search(value : Int) : Int = {
			for(i<-0 until this.pos){
				if(this.priority(i).==(value))
					return i+1	
			}
			return -1
	}

	def changeValue(value : Int, newValue : Int){
		var position = this.search(value)-1
		 (position.!=(-1) && newValue.>(value)) match {
		   case true  => this.priority(position) = newValue
		                 this.reviseAbove(position)
		   case false => this.priority(position) = newValue
		                 this.reviseBelow(position)
			}
	}
	
	def show(){
	  print("N�s: ")
		for(i<- 0 until this.pos) print(this.priority(i)+" ")
		println
	} 

}